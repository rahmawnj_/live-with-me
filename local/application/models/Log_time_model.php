<?php
class Log_time_model extends CI_Model
{
    public function get_log_times()
    {
        return $this->db->get('log_time')->result_array();
    }

    public function get_log_time($field, $id_log_time)
    {
        $this->db->order_by('time', 'asc');
        return $this->db->get_where('log_time', array($field => $id_log_time))->result_array();
    }

    public function insert(...$data)
    {
        $this->db->insert('log_time', $data[0]);
    }

    public function update($id_log_time, ...$data)
    {
        $this->db->update('log_time', $data[0], ['id_log_time' => $id_log_time]);
    }

    public function delete($id_log_time)
    {
        $this->db->where('id_log_time', $id_log_time);
        $this->db->delete('log_time');
    }
}
