<!DOCTYPE html>
<html style="font-size: 16px;" lang="en">

<head>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta charset="utf-8">
  <meta name="keywords" content="Design Perfection">
  <meta name="description" content="">
  <title>Live with me</title>
  <link rel="stylesheet" href="<?= base_url('assets/nicepage.css') ?>" media="screen">
  <script class="u-script" type="text/javascript" src="<?= base_url('assets/jquery.js') ?>" defer=""></script>
  <script class="u-script" type="text/javascript" src="<?= base_url('assets/nicepage.js') ?>" defer=""></script>
  <meta name="generator" content="live with me">
  <meta name="referrer" content="origin">
  <link rel="icon" href="<?= base_url('assets/logo2.png') ?>"  type="image/icon type">
  <meta name="theme-color" content="#478ac9">
  <meta property="og:title" content="LIVE WITH ME">
</head>

<body data-home-page="Home.html" data-home-page-title="Home" class="u-body u-xl-mode" data-lang="en">
  <header class="u-clearfix u-header u-header" id="sec-7a8e">
    <div class="u-clearfix u-sheet u-sheet-1">
      <a href="https://nicepage.com" class="u-image u-logo u-image-1" data-image-width="572" data-image-height="649">
        <img src="<?= base_url('assets/logo.png') ?>" class="u-logo-image u-logo-image-1">
      </a>
      <nav class="u-menu u-menu-one-level u-offcanvas u-menu-1">
        <div class="menu-collapse" style="font-size: 1rem; letter-spacing: 0px;">
          <a class="u-button-style u-custom-left-right-menu-spacing u-custom-padding-bottom u-custom-top-bottom-menu-spacing u-nav-link u-text-active-palette-1-base u-text-hover-palette-2-base"
            href="#">
            <svg class="u-svg-link" viewBox="0 0 24 24">
              <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#menu-hamburger"></use>
            </svg>
            <svg class="u-svg-content" version="1.1" id="menu-hamburger" viewBox="0 0 16 16" x="0px" y="0px"
              xmlns:xlink="http://www.w3.org/1999/xlink" xmlns="http://www.w3.org/2000/svg">
              <g>
                <rect y="1" width="16" height="2"></rect>
                <rect y="7" width="16" height="2"></rect>
                <rect y="13" width="16" height="2"></rect>
              </g>
            </svg>
          </a>
        </div>
       
        <div class="u-custom-menu u-nav-container-collapse">
          <div class="u-black u-container-style u-inner-container-layout u-opacity u-opacity-95 u-sidenav">
            <div class="u-inner-container-layout u-sidenav-overflow">
              <div class="u-menu-close"></div>
             
            </div>
          </div>
          <div class="u-black u-menu-overlay u-opacity u-opacity-70"></div>
        </div>
      </nav>
    </div>
  </header>
  <section class="u-align-left u-clearfix u-palette-5-light-2 u-section-1" id="sec-c520">
    <div class="u-clearfix u-sheet u-sheet-1">
      <div style="margin-top: 70px; margin-bottom: 70px; padding:20px;" class="u-align-center u-container-style u-grey-5 u-group u-group-1">
        <div class="u-container-layout u-container-layout-1">
          <h1 class="u-text u-text-default u-text-1">Live With Me</h1>
          <div class="u-expanded-width u-form u-form-1">
          <?= $this->session->flashdata('message'); ?>

            <form action="<?= base_url('data/sync') ?>" method="post"
               >
              <div class="u-form-group u-form-name u-label-none">
                <label for="name-ef64" class="u-label">Event</label>
                  <select class="u-border-1 u-border-grey-30 u-input u-input-rectangle" name="id_event" onchange="detectChange(this)">
                  <option selected disabled >Pilih Event</option>
                  <?php foreach($events as $event):?>
                    <option value="<?= $event['id_event'] ?>"><?= $event['nama'] ?></option>
                  <?php endforeach?>
                </select>
              </div>
              <div class="u-form-group u-form-submit">
                <button type="submit" id="syncButton" class="u-border-none u-btn u-btn-submit u-button-style u-custom-color-1 u-btn-1">Submit</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </section>


  <footer class="u-align-center u-clearfix u-footer u-grey-80 u-footer" style="padding:20px;" id="sec-c526">
    <div class="u-clearfix u-sheet u-sheet-1">
      <a href="http://livewithme.co.id/"  class="u-small-text u-text u-text-variant u-text-1">
        Live With Me
      </a>
    </div>
  </footer>
  <script>
		function detectChange(selectEvent) {
			console.log(selectEvent.value)
			$.post('<?= base_url('data/search_event/') ?>', {
                id_event: selectEvent.value
            }, function(data, status) {
				console.log(data)
                var datas = JSON.parse(data)
               datas.log_times.forEach(log_time => {
				console.log(log_time)
			   })
			    if(datas.sync_status == 'disabled'){
					document.getElementById("syncButton").disabled = true;
				} else {
					document.getElementById("syncButton").disabled = false;
				}
            })
		}

	</script>
</body>

</html>