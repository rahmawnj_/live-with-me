<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Live With Me</title>
<script src="<?= base_url('assets/jquery.js') ?>"></script>
	<style type="text/css">

	::selection { background-color: #E13300; color: white; }
	::-moz-selection { background-color: #E13300; color: white; }

	body {
		background-color: #fff;
		margin: 40px;
		font: 13px/20px normal Helvetica, Arial, sans-serif;
		color: #4F5155;
	}

	</style>
</head>
<body>
	<form action="<?= base_url('data/sync') ?>" method="post">
		<select name="id_event" onchange="detectChange(this)">
			<?php foreach($events as $event):?>
				<option value="<?= $event['id_event'] ?>"><?= $event['nama'] ?></option>
			<?php endforeach?>
		</select>
		<button id="syncButton">Ok</button>
	</form>

	<script>
		function detectChange(selectEvent) {
			console.log(selectEvent.value)
			$.post('<?= base_url('data/search_event/') ?>', {
                id_event: selectEvent.value
            }, function(data, status) {
				console.log(data)
                var datas = JSON.parse(data)
               datas.log_times.forEach(log_time => {
				console.log(log_time)
			   })
			    if(datas.sync_status == 'disabled'){
					document.getElementById("syncButton").disabled = true;
				} else {
					document.getElementById("syncButton").disabled = false;
				}
            })
		}

	</script>
</body>
</html>
