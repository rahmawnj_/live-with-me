<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Data extends CI_Controller {

	public $url;
	public function __construct()
    {
        parent::__construct();
        $this->load->model(['log_time_model']);
        $this->load->helper(['form', 'url', 'date', 'sync_log_time']);
		$this->url = 'http://localhost/projek/live-with-me/hosting/';
    }
	public function index()
	{
		$url = $this->url . '/api/events';
		$collection_name = 'RapidAPI';
		$request_url = $url . '/' . $collection_name;

		$curl = curl_init($request_url);

		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		$events = json_decode(curl_exec($curl), true);
		curl_close($curl);

		$data = [
			'events' => $events['events']
		];
		
		$this->load->view('index', $data);
	}

	function group_by($key, $data) {
		$result = array();
	
		foreach($data as $val) {
			if(array_key_exists($key, $val)){
				$result[$val[$key]][] = $val;
			}else{
				$result[""][] = $val;
			}
		}
	
		return $result;
	}


    function filter_array($array,$term){
        $matches = array();
        foreach($array as $a){
            if($a['id_waktu'] == $term)
                $matches[]=$a;
        }
        return $matches;
    }

	public function search_event()
	{
		$id_event = $this->input->post('id_event');

		$log_time_event = $this->log_time_model->get_log_time('id_event', $id_event);
	
		if (count($log_time_event) !== 0) {
			$dataSubjectsValue = array_column($log_time_event, 'status');
			if (in_array(0, $dataSubjectsValue)) {
			// if (array_search(0, array_column($log_time_event, 'status'))) {
				$sync_status = 'able';
			} else {
				$sync_status = 'disabled';
			}
			
		} else {
			$sync_status = 'disabled';
		}
		echo json_encode(['log_times' => $log_time_event, 'sync_status' => $sync_status]);
	}

	public function tes()
	{
		$data = array("username" => "TESTAPI", "password" => "APITEST");                                                                    
        $data_string = json_encode($data);                                                                                   

        $ch = curl_init($this->url . '/api/log_times');                                                                      
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);                                                                  
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                                                                      
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
        'Content-Type: application/json',                                                                                
        'Content-Length: ' . strlen($data_string))                                                                       
        );                                                                                                                   

        $result = curl_exec($ch);
	}

	public function sync($id_event)
	{

		
		// $id_event = $this->input->post('id_event');
		$log_time_event = $this->log_time_model->get_log_time('id_event', $id_event);
		$byGroupLogTime = $this->group_by("rfid", $log_time_event);
		$data = array("id_event" => $id_event);
		$data_string = json_encode($data);

		$ch = curl_init($this->url .'/api/rfid');
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(
			'Content-Type: application/json',
			'Content-Length: ' . strlen($data_string)),
		);
		curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY); 
		$result = curl_exec($ch);
		$rfid = (array) json_decode($result);
		$resultRFID = $rfid['rfid'];
	
		$rfids = [];
        if (count($resultRFID) > 0) {
			foreach($resultRFID as $res){
				$rfids[] = $res->rfid;
			}
		} 

		// Delete RFID tidak terdaftar
		foreach ($byGroupLogTime as $key => $log_time) {
			foreach($log_time as $lt){
				if(!in_array($lt['rfid'], $rfids)){
					// $this->log_time_model->delete($lt['id_log_time']);
				}
			}
		}

		foreach($byGroupLogTime as $key => $log_time){
			$ByGroup = [];
			$urutans = [101,102,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48];
			foreach($urutans as $value){
				$new = $this->filter_array($log_time,$value);
				if(count($new) !== 0){
					$this->log_time_model->update($new[0]['id_log_time'], [
						'status' => 1
					]);
					foreach($new as $n){
						if($n['id_log_time'] !== $new[0]['id_log_time']){
							// $this->log_time_model->delete($n['id_log_time']);
						}
					}
					
					$time = $new[0]['time'];
				} else {
					$time = '';
				}
				$ByGroup[$value] = $time;
			}

			$data = array('rfid' => $key,
			"start" => $ByGroup[101], 
			"finish" => $ByGroup[102],
			"cp1" => $ByGroup[1],
			"cp2" => $ByGroup[2],
			"cp3" => $ByGroup[3],
			"cp4" => $ByGroup[4],
			"cp5" => $ByGroup[5],
			"cp6" => $ByGroup[6],
			"cp7" => $ByGroup[7],
			"cp8" => $ByGroup[8],
			"cp9" => $ByGroup[9],
			"cp10" => $ByGroup[10],
			"cp11" => $ByGroup[11],
			"cp12" => $ByGroup[12],
			"cp13" => $ByGroup[13],
			"cp14" => $ByGroup[14],
			"cp15" => $ByGroup[15],
			"cp16" => $ByGroup[16],
			"cp17" => $ByGroup[17],
			"cp18" => $ByGroup[18],
			"cp19" => $ByGroup[19],
			"cp20" => $ByGroup[20],
			"cp21" => $ByGroup[21],
			"cp22" => $ByGroup[22],
			"cp23" => $ByGroup[23],
			"cp24" => $ByGroup[24],
			"cp25" => $ByGroup[25],
			"cp26" => $ByGroup[26],
			"cp27" => $ByGroup[27],
			"cp28" => $ByGroup[28],
			"cp29" => $ByGroup[29],
			"cp30" => $ByGroup[30],
			"cp31" => $ByGroup[31],
			"cp32" => $ByGroup[32],
			"cp33" => $ByGroup[33],
			"cp34" => $ByGroup[34],
			"cp35" => $ByGroup[35],
			"cp36" => $ByGroup[36],
			"cp37" => $ByGroup[37],
			"cp38" => $ByGroup[38],
			"cp39" => $ByGroup[39],
			"cp40" => $ByGroup[40],
			"cp41" => $ByGroup[41],
			"cp42" => $ByGroup[42],
			"cp43" => $ByGroup[43],
			"cp44" => $ByGroup[44],
			"cp45" => $ByGroup[45],
			"cp46" => $ByGroup[46],
			"cp47" => $ByGroup[47],
			"cp48" => $ByGroup[48],
		);                                                                    
        $data_string = json_encode($data);                                                                                   

        $ch = curl_init( $this->url . '/api/log_times');                                                                      
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);                                                                  
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                                                                      
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
        'Content-Type: application/json',                                                                                
        'Content-Length: ' . strlen($data_string))                                                                       
        );                                                                                                                   

        $result = curl_exec($ch);
       
		}
		$this->session->set_flashdata('message', 'Sync telah dikirim!');
		redirect('/');
	}
}
